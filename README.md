# Gestão de validade da credencial de acesso

Este projeto apresenta uma abordagem de gerenciamento do tempo de vida das credenciais emitidas pela plataforma da BRy ([BRy Cloud]).

A abordagem apresenta um gerenciamento ativo, onde uma tarefa realiza validações períodicas do período de vigência do access token armazenado em memória.

O access token armazenado em memória válido e monitorado é disponibilizado na forma de serviço através do endereço: http://localhost:8000/jwt

Abaixo seguem os passos principais:
* Ao iniciar, a aplicação exemplo requisita um **Access Token** para a plataforma de serviços da BRy, utilizando as credenciais de uma aplicação cadastrada: **client_id** e **client_secret**.  
* Em um período configurável, a tarefa desperta e realiza a validação do tempo de vida do Access Token (processo de decodifição do JWT e extração da claim **exp**).
* Sempre que o Access Token estiver próximo de sua expiração, a tarefa dispara o evento de renovação da credencial utilizando o **Refresh Token**.

**Observação**

Para que a aplicação realize a validação de tempo de vida do Access Token, é necessário adicionar o seguinte comando no arquivo Cron do seu servidor/sistema:

**"* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1"**

### Tech

O exemplo utiliza das bibliotecas PHP abaixo:
* [PHP 7.4] - PHP is a popular general-purpose scripting language that is especially suited to web development.
* [Laravel] - The PHP Framework for Web Artisans.
* [CURL] - Client URL Library.

**Observação**

É necessário ter o [Laravel] e todas suas dependências instalados na sua máquina, além do [Composer] para a instalação das dependências e execução do projeto.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com os dados de conexão e identificação da aplicação cliente.

| Variável  |                   Descrição                   | Arquivo de Configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| url | Access Token para o consumo do serviço (JWT). |     TokenController.php    |
| clientId | Access Token para o consumo do serviço (JWT). |     TokenController.php    |
| clientSecret | Access Token para o consumo do serviço (JWT). |     TokenController.php    |

### Observações

Os atributos de data e hora codificados nos tokens JWT (access token e refresh token), seguem o fuso horário UTC-0.
Lembrando que o Brasil possui 4 fusos horários:
* UTC-2 - Atol das Rocas e Fernando de Noronha.
* UTC-3 **(oficial)** - Estados não citados nos demais itens.
* UTC-4 - Amazonas, Mato Grosso, Mato Grosso do Sul, Rondônia e Roraima.
* UTC-5 - Acre e alguns municípios do Amazonas.

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o [Composer] para a instalação das dependências.

Comandos:

Instalar dependências:

    -composer install 

Executar programa:

    -php artisan serve
    

  [BRy Cloud]: <https://cloud.bry.com.br>
  [PHP 7.4]: <https://www.php.net/releases/7_4_0.php>
  [Laravel]: <https://laravel.com/>
  [CURL]: <https://www.php.net/manual/pt_BR/book.curl.php>
  [Composer]: <https://getcomposer.org/>
