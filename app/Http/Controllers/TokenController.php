<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TokenController extends Controller
{

    // URLs de geração do Token
    const url = "URL do serviço de geração de tokens da BRy";
    const clientId = "Client ID da aplicação gerado no BRy Cloud";
    const clientSecret = "Client Secret da aplicação gerado no BRy Cloud";

    public function generateAccessToken(Request $request)
    {
        if (Cache::has('jwt')) {
            self::checkToken();
            $jwt = Cache::get('jwt');
        } else {
            $jwt = self::postAccessTokenRequest();
            Cache::put('jwt', $jwt,  now()->addMinutes(($jwt->expires_in) / 60));
        }

        return response()->json($jwt);
    }

    public function checkToken() {
        if (!Cache::has('jwt')) {
            $jwt = self::postAccessTokenRequest();
            Cache::put('jwt', $jwt,  now()->addMinutes(($jwt->expires_in) / 60));
        } else {
            $jwt = Cache::get('jwt');
            $exp = self::decodeToken($jwt->access_token)->exp;

            if (($exp - time()) < 180) {
                $newJwt = self::renewAccessTokenRequest($jwt->refresh_token);
                Cache::put('jwt', $newJwt,  now()->addMinutes(($jwt->expires_in) / 60));
            }
        }
    }

    public function postAccessTokenRequest(){
        $data = array(
              'grant_type'=>'client_credentials',
              'client_id'=>self::clientId,
              'client_secret'=>self::clientSecret);

        $query = http_build_query($data);

        $curlToken = curl_init();

        curl_setopt_array($curlToken, array(
            CURLOPT_URL => self::url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $query,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        // ENVIA A REQUISIÇÃO
        $respostaToken = curl_exec($curlToken);
        curl_close($curlToken);

        $respostaToken = json_decode($respostaToken);
        
        return $respostaToken;
    }

    public function renewAccessTokenRequest($refreshToken){
        $data = array(
              'grant_type'=>'refresh_token',
              'refresh_token'=>$refreshToken);

        $query = http_build_query($data);

        $curlToken = curl_init();

        curl_setopt_array($curlToken, array(
            CURLOPT_URL => self::url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $query,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        // ENVIA A REQUISIÇÃO
        $respostaToken = curl_exec($curlToken);
        curl_close($curlToken);

        $respostaToken = json_decode($respostaToken);
        
        return $respostaToken;
    }

    public function decodeToken($token) {
        $tokenParts = explode(".", $token);  
        $tokenHeader = base64_decode($tokenParts[0]);
        $tokenPayload = base64_decode($tokenParts[1]);
        $jwtHeader = json_decode($tokenHeader);
        $jwtPayload = json_decode($tokenPayload);
        return $jwtPayload;
    }
}
